
<!-- PROJECT LOGO -->

  <h3 align="center">NASA Facts React.js Page</h3>

  <p align="center">
    A simple react.js page with some nice Facts from NASA.
    <br />
  </p>
</p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#contact">Contact</a></li>    
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

<p align="center">
  <img src="src/images/NasaFunFacts.gif" alt="Nasa Facts Screen"/>
</p>


During the React: Building Styles with CSS Modules course, I built this application. Some concepts learned:
- Setting up React with CSS modules
- Navigating the directory structure
- Adding components to an app
- Using CSS syntax with components
- Leveraging other styles
- Using grid components
- Developing interactions and responsiveness
- Implementing media queries
- Combining styles

[Course Link](https://www.linkedin.com/learning/react-building-styles-with-css-modules/welcome?u=2201753)

### Built With

This was you built using.

- [React Js](https://reactjs.org/)
- [create-react-app](https://github.com/facebook/create-react-app)

<!-- GETTING STARTED -->

## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.

- yarn
  ```sh
  npm install --global yarn
  ```

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/osvaldolpjunior/nasa-facts.git
   ```
2. Install Yarn packages
   ```sh
   yarn install
   ```
3. Execute using Yarn
   ```
   yarn start
   ```

<!-- CONTACT -->

## Contact

Osvaldo Junior - osvaldo.junior@and.digital

Project Link: [https://gitlab.com/osvaldolpjunior/nasa-facts](https://gitlab.com/osvaldolpjunior/nasa-facts)

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
