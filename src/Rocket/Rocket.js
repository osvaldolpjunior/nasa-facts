import React from 'react';
import styles from '../Rocket.module.css';
import shuttle from '../images/shuttle.png';

export default class Rocket extends React.Component {
  render() {
    return (
      <div>
        <img src={shuttle} alt="shuttle" className={styles.img} />
      </div>
    );
  }
}
