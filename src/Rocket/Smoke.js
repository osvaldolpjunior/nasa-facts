import React from 'react';
import styles from '../Smoke.module.css';
import smoke from '../images/smoke.mov';

export default class Smoke extends React.Component {
  render() {
    return (
      <div className={styles.smoke}>
        <video className={styles.video} autoPlay loop src={smoke}></video>
      </div>
    );
  }
}
