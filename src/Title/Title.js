import React from 'react';
import styles from '../Title.module.css';

const Title = (props) => {
  return (
    <div>
      <h1 className={styles.title}>NASA Facts</h1>
    </div>
  );
}

export default Title;